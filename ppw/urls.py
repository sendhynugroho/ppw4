from django.urls import path
from . import views
from .views import index
#url for app
urlpatterns = [
    path('', views.index, name='index'),
    path('about/', views.about, name='about'),
    path('cv/', views.cv, name='cv'),
    path('form/', views.form, name='form'),
]
