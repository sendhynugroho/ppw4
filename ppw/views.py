from django.shortcuts import render, get_object_or_404
from datetime import datetime, date
# Enter your name here
mhs_name = 'Sendhy Nugroho' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 1, 29) #TODO Implement this, format (Year, Month, Date)
npm = 170604363222 # TODO Implement this
# Create your views here.
def index(request):
    return render(request, 'Home.html')

def about(request):
    return render(request, 'About.html')

def cv(request):
    return render(request, 'CV.html')

def form(request):
    return render(request, 'Form.html')
